<?php

namespace wise5lin\ckeditor4;

/*
 *          _)             __|  | _)
 * \ \  \ / | (_-<   -_) __ \  |  |    \
 *  \_/\_/ _| ___/ \___| ___/ _| _| _| _|
 *
 * @author Двуреченский Сергей
 * @link   <wise5lin@yandex.ru>
 */

use yii\helpers\Html;
use yii\helpers\Json;
use yii\helpers\ArrayHelper;
use yii\widgets\InputWidget;
use yii\base\InvalidConfigException;

/**
 * Виджет для редактора `CKEditor 4`.
 *
 * @see https://ckeditor.com/ckeditor-4
 *
 * @example https://ckeditor.com/docs/ckeditor4/latest/examples/index.html
 *
 * ИСПОЛЬЗОВАНИЕ:
 *
 * use wise5lin\ckeditor4\CKEditor;
 *
 * <?= $form->field($model, 'content')->widget(CKEditor::class) ?>
 */
class CKEditor extends InputWidget
{
    /**
     * Выбор предустановленных настроек для панели редактора.
     *
     * basic, standard, full, custom
     *
     * @var string
     */
    public $preset = 'standard';
    /**
     * Использовать редактор как встроенный в страницу.
     *
     * @var bool
     */
    public $inline = false;
    /**
     * Js опции редактора `CKEditor 4`, все возможные опции смотрите на официальном сайте -
     * "https://ckeditor.com/docs/ckeditor4/latest/api/CKEDITOR_config.html".
     *
     * @var array
     */
    public $editorOptions = [];
    /**
     * HTML тег контейнера оборачивающего редактор.
     *
     * @var string
     */
    public $editorContainerTag = 'div';
    /**
     * HTML атрибуты для контейнера оборачивающего редактор.
     *
     * @var array
     */
    public $editorContainerOptions = [];

    //   _ \ _)   _| _|                     |       __|            |
    //   |  | |   _| _| -_)   _| -_)    \    _|    (      _ \   _` |   -_)
    //  ___/ _| _| _| \___| _| \___| _| _| \__|   \___| \___/ \__,_| \___|

    /**
     * {@inheritdoc}
     *
     * @throws InvalidConfigException
     */
    public function init()
    {
        parent::init();

        if (!is_array($this->editorOptions)) {
            throw new InvalidConfigException('The "editorOptions" property must be an array.');
        }

        if (!is_array($this->editorContainerOptions)) {
            throw new InvalidConfigException('The "editorContainerOptions" property must be an array.');
        }

        $this->id = $this->options['id'];

        switch ($this->preset) {
            case 'basic':
                $preset = $this->basicPreset();
                break;
            case 'full':
                $preset = $this->fullPreset();
                break;
            case 'custom':
                $preset = [
                    'toolbarGroups' => [],
                    'removeButtons' => null,
                    'removePlugins' => null,
                    'removeDialogTabs' => null,
                ];
                break;
            default:
                $preset = $this->standardPreset();
                break;
        }

        $this->editorOptions = ArrayHelper::merge($preset, $this->editorOptions);
        $this->editorContainerOptions = ArrayHelper::merge([
            'id' => $this->inline ? "{$this->id}-editor-inline" : "{$this->id}-editor",
        ], $this->editorContainerOptions);
    }

    /**
     * {@inheritdoc}
     */
    public function run()
    {
        $textarea = Html::textarea($this->name, $this->value, $this->options);

        if ($this->hasModel()) {
            $textarea = Html::activeTextarea($this->model, $this->attribute, $this->options);
        }

        echo Html::tag($this->editorContainerTag, $textarea, $this->editorContainerOptions);

        $this->registerClientScript();
    }

    /**
     * Базовые настройки панели редактора.
     *
     * @return array
     */
    private function basicPreset()
    {
        return [
            'height' => 200,
            'toolbarGroups' => [
                ['name' => 'undo'],
                ['name' => 'basicstyles'],
                ['name' => 'list', 'groups' => ['list', 'indent']],
                ['name' => 'links'],
                ['name' => 'about'],
            ],
            'removeButtons' => 'Undo,Redo,Underline,Strike,Subscript,Superscript,Anchor,Flash,Table,HorizontalRule,Smiley,SpecialChar,PageBreak,Iframe',
            'removeDialogTabs' => 'link:advanced',
        ];
    }

    /**
     * Стандартные настройки панели редактора.
     *
     * @return array
     */
    private function standardPreset()
    {
        return [
            'height' => 300,
            'toolbarGroups' => [
                ['name' => 'clipboard', 'groups' => ['clipboard', 'undo', 'spellchecker']],
                ['name' => 'links'],
                ['name' => 'insert'],
                ['name' => 'tools', 'groups' => ['tools', 'mode']],
                '/',
                ['name' => 'basicstyles', 'groups' => ['basicstyles', 'cleanup']],
                ['name' => 'paragraph', 'groups' => ['list', 'indent', 'blocks']],
                ['name' => 'styles'],
                ['name' => 'about'],
            ],
            'removeButtons' => 'Underline,Subscript,Superscript,PageBreak,CopyFormatting,ShowBlocks,CreateDiv,Font,FontSize,Smiley,Iframe',
            'removeDialogTabs' => 'image:advanced;link:advanced',
        ];
    }

    /**
     * Максимальные настройки панели редактора.
     *
     * @return array
     */
    private function fullPreset()
    {
        return [
            'height' => 400,
            'toolbarGroups' => [
                ['name' => 'document', 'groups' => ['mode', 'document', 'doctools']],
                ['name' => 'clipboard', 'groups' => ['clipboard', 'undo']],
                ['name' => 'editing', 'groups' => ['find', 'selection', 'spellchecker']],
                ['name' => 'forms'],
                '/',
                ['name' => 'basicstyles', 'groups' => ['basicstyles', 'cleanup']],
                ['name' => 'paragraph', 'groups' => ['list', 'indent', 'blocks', 'align', 'bidi']],
                ['name' => 'links'],
                ['name' => 'insert'],
                '/',
                ['name' => 'styles'],
                ['name' => 'blocks'],
                ['name' => 'colors'],
                ['name' => 'tools'],
                ['name' => 'about'],
            ],
        ];
    }

    /**
     * Регистрирует CSS и JS файлы на странице.
     */
    private function registerClientScript()
    {
        $view = $this->getView();

        CKEditorAsset::register($view);

        $editorOptions = Json::encode($this->editorOptions);

        $registerJs = $this->inline ? "CKEDITOR.inline('{$this->id}', {$editorOptions});" :
            "CKEDITOR.replace('{$this->id}', {$editorOptions});";

        $view->registerJs($registerJs);
    }
}
