CKEditor 4 виджет для Yii2 Framework
====================================
Виджет для CKEditor 4.

Installation
------------

The preferred way to install this extension is through [composer](http://getcomposer.org/download/).

Either run

```
php composer.phar require --prefer-dist wise5lin/yii2-ckeditor4 "*"
```

or add

```
"wise5lin/yii2-ckeditor4": "*"
```

to the require section of your `composer.json` file.


Usage
-----

Once the extension is installed, simply use it in your code by  :

```php
<?= \wise5lin\ckeditor4\CKEditor::widget(); ?>```
